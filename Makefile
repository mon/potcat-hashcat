NAME = potcat-hashcat

default:

install:
	install -p -m 755 $(NAME) $(DESTDIR)/usr/local/bin/$(NAME)

clean:
	rm -f *~ *tar.gz
